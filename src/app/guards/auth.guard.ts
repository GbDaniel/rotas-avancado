import { LoginService } from './../login/login.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private loginService: LoginService,
    private router: Router){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      if(this.usuarioAutenticado()){
        return this.usuarioAutenticado();
      }else{
        this.router.navigate(['/login']);
        return this.usuarioAutenticado();
      }
  }

  usuarioAutenticado(): boolean{
    return this.loginService.isUsuarioAutenticado();
  }

  canLoad(route: Route, segments: UrlSegment[]): boolean | Promise<boolean> | Observable<boolean> {
      return this.usuarioAutenticado();
  }

}

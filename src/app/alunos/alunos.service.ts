import { Injectable } from '@angular/core';
import { Aluno } from '../shared/Aluno';

@Injectable({
  providedIn: 'root'
})
export class AlunosService {

  private alunos: Aluno[] = [
    {id: 1, nome: 'Aluno 01', email: 'aluno01@email.com'},
    {id: 2, nome: 'Aluno 02', email: 'aluno02@email.com'},
    {id: 3, nome: 'Aluno 03', email: 'aluno03@email.com'},
    {id: 4, nome: 'Aluno 04', email: 'aluno04@email.com'}
  ]

  constructor() { }

  getAlunos(){
    return this.alunos;
  }

  getAluno(alunoId: number){
    for (let index = 0; index < this.alunos.length; index++) {
      let aluno = this.alunos[index];
      if(alunoId == aluno.id){
        return aluno;
      }
    }
  }

  setAluno(aluno: any){
    let alunoServidor = this.getAluno(aluno.id);
    if(alunoServidor == null){
      this.alunos.push(aluno);
    } else {
      for (let index = 0; index < this.getAlunos.length; index++) {
        if(aluno.id = this.getAlunos[index].id){
          this.alunos[index] = aluno;
        }
      }
    }

  }
}

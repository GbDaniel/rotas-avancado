import { Aluno } from './../../shared/Aluno';
import { Subscription } from 'rxjs';
import { AlunosService } from './../alunos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aluno-detalhe',
  templateUrl: './aluno-detalhe.component.html',
  styleUrls: ['./aluno-detalhe.component.css']
})
export class AlunoDetalheComponent implements OnInit {

  private aluno: Aluno;
  private inscricao: Subscription;

  constructor(private router: Router,
    private rotaAtviva: ActivatedRoute,
    private alunosService: AlunosService) { }

  ngOnInit() {
    /* Comentando código que pega da rota para executar código que pega do resolver */
    /*
    this.inscricao = this.rotaAtviva.params.subscribe(
      params => {
        this.aluno = this.alunosService.getAluno(params['id']);
        if(this.aluno == null){
          this.router.navigate(['alunos','alunoNaoEncontrado']);
        }
      });
    */

      /* Usando resolver */
    this.inscricao = this.rotaAtviva.data.subscribe(
      params => {
        this.aluno = params.alunoResolver;
        if(this.aluno == null){
          this.router.navigate(['alunos','alunoNaoEncontrado']);
        }
    });
  }

  editarAluno(){
    this.router.navigate(['alunos', this.aluno.id, 'editar']);
  }

  voltar(){
    this.router.navigate(['alunos']);
  }

  ngOnDestroy(){
    this.inscricao.unsubscribe();
  }
}

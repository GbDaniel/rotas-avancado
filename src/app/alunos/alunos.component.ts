import { Aluno } from './../shared/Aluno';
import { AlunosService } from './alunos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alunos',
  templateUrl: './alunos.component.html',
  styleUrls: ['./alunos.component.css']
})
export class AlunosComponent implements OnInit {

  private alunos: Aluno[];

  constructor(private alulnosService:AlunosService) {

  }

  ngOnInit() {
    this.alunos = this.alulnosService.getAlunos();
  }

}

import { CanDeactivateGuard } from '../guards/can-deactivate.guard';
import { AlunosGuard } from './../guards/alunos.guard';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';
import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlunosComponent } from './alunos.component';
import { AlunoNaoEncontradoComponent } from './aluno-nao-encontrado/aluno-nao-encontrado.component';
import { AlunosResolver } from '../resolvers/alunos-resolver';

/* Para mestre detalhe acrescenta-se o <router-outlet></router-outlet>
  no template principal e usa-se:
const alunoRoutes: Routes = [
  {path: 'alunos', component: AlunosComponent, children: [
    {path: 'novo', component: AlunoFormComponent},
    {path: ':id', component: AlunoDetalheComponent},
    {path: ':id/editar', component: AlunoFormComponent}
  ]}
];

  Nesse modelo de rotas filhas, também podemos usar os guardas de rotas filhas.

*/

const alunoRoutes: Routes = [
  {path: '', component: AlunosComponent},
  {path: 'novo', component: AlunoFormComponent, canDeactivate: [CanDeactivateGuard]},
  {path: 'alunoNaoEncontrado', component: AlunoNaoEncontradoComponent},
  {path: ':id', component: AlunoDetalheComponent, resolve: {alunoResolver: AlunosResolver}},
  {path: ':id/editar', component: AlunoFormComponent, canActivate: [AlunosGuard], canDeactivate: [CanDeactivateGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(alunoRoutes)],
  exports: [RouterModule]
})
export class AlunosRoutingModule { }

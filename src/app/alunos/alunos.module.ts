import { AlunosService } from './alunos.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlunosRoutingModule } from './alunos-routing.module';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';
import { AlunosComponent } from './alunos.component';
import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';
import { RouterModule } from '@angular/router';
import { AlunoNaoEncontradoComponent } from './aluno-nao-encontrado/aluno-nao-encontrado.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    RouterModule,
    AlunosRoutingModule
  ],
  declarations: [
    AlunoFormComponent,
    AlunosComponent,
    AlunoDetalheComponent,
    AlunoNaoEncontradoComponent],
  providers: [
    AlunosService
  ]
})
export class AlunosModule { }

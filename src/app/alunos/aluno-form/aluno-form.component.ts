import { Aluno } from './../../shared/Aluno';
import { FormCanDeactivate } from './../../guards/form-candeactivate';
import { AlunosService } from './../alunos.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aluno-form',
  templateUrl: './aluno-form.component.html',
  styleUrls: ['./aluno-form.component.css']
})
export class AlunoFormComponent implements FormCanDeactivate, OnInit {

  private aluno: Aluno;
  private inscricao: Subscription;
  formMudou: boolean = false;

  constructor(private router: Router,
    private rotaAtiva: ActivatedRoute,
    private alunosService: AlunosService) { }

  ngOnInit() {
    this.inscricao = this.rotaAtiva.params.subscribe(
      params=>{
        this.aluno = this.alunosService.getAluno(params['id']);
        if(this.aluno == null){
          this.router.navigate(['alunos','alunoNaoEncontrado']);
        }
    });
  }

  salvarAluno(){
    this.alunosService.setAluno(this.aluno);
    this.formMudou = false;
    this.router.navigate(['alunos',this.aluno.id]);
  }

  onInput(){
    this.formMudou = true;
  }

  podeMudarRota(){
    if(this.formMudou){
      return confirm('Tem certeza que deseja sair sem salvar');
    } else {
      return true;
    }
  }

  ngOnDestroy(){
    this.inscricao.unsubscribe();
  }

}

import { AuthGuard } from './guards/auth.guard';
import { FormsModule } from '@angular/forms';
import { LoginService } from './login/login.service';
import { CursosService } from './cursos/cursos.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MaterializeModule } from 'angular2-materialize';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MaterializeModule,
    AppRoutingModule
  ],
  providers: [
    CursosService,
    LoginService,
    AuthGuard,
    CursosService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }

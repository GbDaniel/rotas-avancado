import { AlunosService } from './../alunos/alunos.service';
import { Aluno } from './../shared/Aluno';
import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlunosResolver implements Resolve<Aluno>{

  constructor(private alunosService: AlunosService){

  }

  resolve(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Aluno | Observable<Aluno> | Promise<Aluno> {

      let id = route.params['id'];
      return this.alunosService.getAluno(id);
  }

}

import { CursosService } from '../cursos.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-curso-detalhe',
  templateUrl: './curso-detalhe.component.html',
  styleUrls: ['./curso-detalhe.component.css']
})
export class CursoDetalheComponent implements OnInit {

  curso: any;

  inscricao: Subscription;

  constructor(private rotaAtiva: ActivatedRoute, private router: Router, private cursosService: CursosService) {
  }

  ngOnInit() {
    this.inscricao = this.rotaAtiva.params.subscribe(params => {
      this.curso = this.cursosService.getCurso(params['id']);
      if(this.curso == null){
        this.router.navigate(['cursos', 'naoEncontrado']);
      }
    });
  }

  ngOnDestroy(){
    this.inscricao.unsubscribe();
  }
}

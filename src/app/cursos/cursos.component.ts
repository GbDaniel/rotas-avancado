import { Router, ActivatedRoute } from '@angular/router';
import { CursosService } from './cursos.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {

  cursos: any[];

  pagina: number;
  totalPagina: number;
  proximaPagina: boolean;
  paginaAnterior: boolean;
  itensPorPagina: number = 3;

  inscricao: Subscription;

  constructor(
      private rotaAtiva: ActivatedRoute,
      private router: Router,
      private cursosService: CursosService) { }

  ngOnInit() {
    /*contadores carregados manualmente porque é só um exemplo.
      em casos reais, os dados seriam baseados na consulta no banco*/
    this.totalPagina = 2
    this.inscricao = this.rotaAtiva.queryParams.subscribe(
      params => {
        this.pagina = params['pagina'];
        if(!this.pagina){
          this.pagina = 1;
        }
        //calcula os dados de acordo com a página
        let inicio = (this.pagina - 1) * 3;
        let fim = inicio + 3;
        this.cursos = this.cursosService.getCursos();
        this.cursos = this.cursos.slice(inicio,fim);
        this.paginaAnterior = this.pagina > 1;
        this.proximaPagina = this.pagina < this.totalPagina;
      }
    );
  }

  anterior(){
    this.router.navigate(['/cursos'], {queryParams: {pagina: --this.pagina}});
  }

  proxima(){
    this.router.navigate(['/cursos'], {queryParams: {pagina: ++this.pagina}});
  }

  ngOnDestroy(){
    this.inscricao.unsubscribe();
  }
}

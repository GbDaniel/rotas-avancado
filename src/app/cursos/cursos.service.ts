import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  cursos: any[] = [
    {id: 1, nome: 'Angular'},
    {id: 2, nome: 'Spring'},
    {id: 3, nome: 'JavaEE'},
    {id: 4, nome: 'PHP'},
    {id: 4, nome: 'Docker'}
  ];

  constructor() { }

  getCursos(){
    return this.cursos;
  }

  getCurso(id: number){
    for (let index = 0; index < this.cursos.length; index++) {
      let c = this.cursos[index];
      if(c.id == id){
        return c;
      }
    }
  }

}

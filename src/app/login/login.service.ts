import { Router } from '@angular/router';
import { EventEmitter, Injectable } from '@angular/core';
import { Usuario } from '../shared/Usuario';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private usuarioAutenticado: boolean = false;

  mostrarMenu = new EventEmitter<boolean>();

  constructor(private router: Router) { }

  fazerLogin(usuario: Usuario) {
    if(usuario.email === 'usuario@email.com' && usuario.senha === '123456'){
      this.usuarioAutenticado = true;
      this.mostrarMenu.emit(true);
      this.router.navigate(['/']);
    } else {
      this.mostrarMenu.emit(false);
      this.usuarioAutenticado = false;
    }
  }

  isUsuarioAutenticado(){
    return this.usuarioAutenticado;
  }
}

import { LoginService } from './login/login.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rotas-loiane';
  exibeMenu: boolean;

  constructor( private loginService: LoginService){}

  ngOnInit(){
    this.loginService.mostrarMenu.subscribe(
      valor => {this.exibeMenu = valor;}
    );
  }
}
